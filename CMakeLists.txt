project(HPP)
cmake_minimum_required(VERSION 2.8)

add_definitions(
    -std=c++11 # support for new cpp standard
)


#find BOOST components
find_package(Boost COMPONENTS
    program_options
    random
    REQUIRED)

include_directories(${Boost_INCLUDE_DIR})
message(STATUS "Boost libraries:" ${Boost_LIBRARIES})

#find OpenMP for parallel operations
find_package( OpenMP REQUIRED )

message(STATUS "OpenMP_CXX_FLAGS: " ${OpenMP_CXX_FLAGS})
message(STATUS "OpenMP_C_FLAGS: " ${OpenMP_C_FLAGS})
message(STATUS "Release Flags" ${CMAKE_CXX_FLAGS_RELEASE})
message(STATUS "Debug Flags" ${CMAKE_CXX_FLAGS_DEBUG})
set(CMAKE_CXX_FLAGS ${OpenMP_CXX_FLAGS})


#finde OpenGL for display
find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
include_directories( ${OPENGL_INCLUDE_DIRS}  ${GLUT_INCLUDE_DIRS} )


#files from inc folder
set(INC
inc/Node.hpp
inc/Grid.hpp
inc/Renderer.hpp
inc/HPPDynamics

)
#project include dir
include_directories(inc)
#src directory
aux_source_directory(./src SRC_LIST)

#compilation
add_executable(${PROJECT_NAME} ${SRC_LIST} ${INC})
#linking
target_link_libraries("${PROJECT_NAME}"
                            ${OpenMP_CXX_FLAGS}
                            ${Boost_LIBRARIES}
                            ${OPENGL_LIBRARIES}
                            ${GLUT_LIBRARIES}
                     )

