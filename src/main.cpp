#include <iostream>

#include "Renderer.hpp"
#include "Grid.hpp"
#include "HPPDynamics.hpp"
#include "boost/random.hpp"

void set_grid(Grid& g, const int x, const int y)
{

    boost::random::mt19937 rnd;
    rnd.seed(time(0));

    boost::uniform_int<> zero_one(0, 1);
    boost::variate_generator< boost::random::mt19937, boost::uniform_int<> >
                        dice(rnd, zero_one);

    for(int i = 0; i < x; i++)
    {
        for(int j = 0; j < y; j++)
        {
            auto& n = g.getNode(i, j);
            n.set(0, dice());
            n.set(1, dice());
            n.set(2, dice());
            n.set(3, dice());

        }
    }


    int cx = x/3;
    int cy = y/4;

    for(int i = 0; i < 90; i++)
        for(int j = 0; j < 90; j++)
    {
        auto& n = g.getNode(cx+i, cy+j);
        n.set(0, 0);
        n.set(1, 0);
        n.set(2, 0);
        n.set(3, 0);
    }



}


Renderer* Renderer::instance = NULL;


int main(int argc, char* argv[])
{
    int N = 750;

    Grid g(N, N);
    Grid g2(N, N);

    set_grid(g, N, N);

    Renderer renderer(g);
    renderer.init(argc, argv);
    renderer.run();

    return 0;
}

