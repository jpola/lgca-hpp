#ifndef __GRID_HPP_
#define __GRID_HPP_

#include <vector>

#include "Node.hpp"

class Grid
{
    using CELLS = std::vector<Node>;
public:
    Grid() : _x_size(0), _y_size(0), _nodes(_x_size*_y_size)
    {

    }

    Grid(int x, int y) : _x_size(x), _y_size(y), _nodes(_x_size * _y_size)
    {

    }

    int getXsize() const
    {
        return _x_size;
    }

    int getYsize() const
    {
        return _y_size;
    }

    Node& getNode(int x, int y)
    {
        x = (x + _x_size) % _x_size;
        y = (y + _y_size) % _y_size;
        //std::cout << "x = " << x << " y = " << y << std::endl;
        return _nodes[x + _x_size * y];
    }
    const Node& getNode(int x, int y) const
    {
        x = (x + _x_size) % _x_size;
        y = (y + _y_size) % _y_size;
        //std::cout << "cst x = " << x << " y = " << y << std::endl;
        return _nodes[x + _x_size * y];
    }

    void setNode(int x, int y, Node n)
    {
        x = (x + _x_size) % _x_size;
        y = (y + _y_size) % _y_size;

        _nodes[x + _x_size * y] = n;
    }

    CELLS& getCells()
    {
        return _nodes;
    }

    const CELLS& getCells() const
    {
        return _nodes;
    }

    CELLS getNeighbours(int x, int y)
    {
        CELLS neighbours(4);

        neighbours[0] = getNode( x - 1, y    );
        neighbours[1] = getNode( x    , y + 1);
        neighbours[2] = getNode( x + 1, y    );
        neighbours[3] = getNode( x    , y - 1);

        return neighbours;
    }


private:
    int _x_size;
    int _y_size;

    std::vector<Node> _nodes;

};

#endif //__GRID_HPP_
