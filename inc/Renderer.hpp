#ifndef __RENDERER_HPP_
#define __RENDERER_HPP_

#include <GL/freeglut.h>
#include "Grid.hpp"

class Renderer
{

public:
    Renderer(const Grid& g)
    {
        this->grid = g;
        g2 = grid;
        image = std::vector<GLubyte>(4 * this->grid.getXsize() * this->grid.getYsize());
    }

    ~Renderer()
    {
        glDeleteTextures(1, &t);
    }

    void init(int argc, char* argv[])
    {
        instance = this;
        // inicjalizacja biblioteki GLUT
        glutInit (&argc, argv);

        // inicjalizacja bufora ramki
        glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);

        // rozmiary głównego okna programu
        glutInitWindowSize (400, 400);

        // utworzenie głównego okna programu
        glutCreateWindow ("HPP LGCA");

        t = createTexture();
    }

    void run()
    {

        // dołączenie funkcji generującej scenę 3D
        glutDisplayFunc (displayWrapper);

        // dołączenie funkcji wywoływanej przy zmianie rozmiaru okna
        glutReshapeFunc (reshapeWrapper);

        glutIdleFunc(idleWrapper);

        // wprowadzenie programu do obsługi pętli komunikatów
        glutMainLoop();

    }

    void update()
    {
        Collide();
        Propagate();
        updateImage();

        glutPostRedisplay();

    }

    static void idleWrapper()
    {
        instance->update();
    }

    static void displayWrapper()
    {
        instance->Display();
    }

    static void reshapeWrapper(int width, int height )
    {
        instance->Reshape(width, height);
    }

private:
    Grid grid, g2;
    GLuint t;

    std::vector<GLubyte> image;

    static Renderer* instance;

    void Display()
    {
        // kolor tła - zawartość bufora koloru
        glClearColor( 0.0, 0.0, 0.0, 0.0 );

        // czyszczenie bufora koloru
        glClear( GL_COLOR_BUFFER_BIT );


        // setup texture mapping
        glEnable(GL_TEXTURE_2D);

        //glBindTexture(GL_TEXTURE_2D, texture);
        glBindTexture(GL_TEXTURE_2D, t);

        glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex2f(-1.0f, -1.0f);
        glTexCoord2f(1.0, 0.0); glVertex2f( 1.0f, -1.0f);
        glTexCoord2f(1.0, 1.0); glVertex2f( 1.0f,  1.0f);
        glTexCoord2f(0.0, 1.0); glVertex2f(-1.0f,  1.0f);
        glEnd();

        glutSwapBuffers();
        glDisable(GL_TEXTURE_2D);
    }

    //zmiana wielkości okna
    void Reshape( int width, int height )
    {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        glViewport( 0, 0, width, height );
        // generowanie sceny 3D
        //Display();
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

    }

    void Propagate()
    {

        g2 = grid;
        #pragma omp parallel for
        for (int y = 0; y < grid.getYsize(); y++)
        {
            for (int x = 0; x < grid.getXsize(); x++)
            {
    //            auto n = g.getNeighbours(x, y);
                auto& c = grid.getNode(x, y);

                auto& left = g2.getNode(x - 1, y);
                auto& right = g2.getNode(x + 1, y);
                auto& top = g2.getNode(x, y+1);
                auto& bottom = g2.getNode(x, y-1);

                bottom.set(0, c.get(0)); c.set(0, 0);
                left.set  (1, c.get(1)); c.set(1, 0);
                top.set   (2, c.get(2)); c.set(2, 0);
                right.set (3, c.get(3)); c.set(3, 0);
            }
        }

        std::swap(grid, g2);
        //grid = g2;
    }

    void Collide()
    {
        g2 = grid;
        #pragma omp parallel for
        for (int y = 0; y < grid.getYsize(); y++)
        {
            for (int x = 0; x < grid.getXsize(); x++)
            {
    //            auto n = g.getNeighbours(x, y);
                auto& c = grid.getNode(x, y);
                auto& c2 = g2.getNode(x, y);

                auto d = c.get_density();
                if (d == 2)
                {

//                    std::cout<< "before: "
//                             << +c.get(0) << " " << +c.get(1)
//                              << " " << +c.get(2) << " " << +c.get(3)
//                              << std::endl;
                    if( (c.get(0) == 0 && c.get(1) == 1 && c.get(2) == 0 && c.get(3) == 1) ||
                        (c.get(0) == 1 && c.get(1) == 0 && c.get(2) == 1 && c.get(3) == 0) )
                    {

                        c2.set(0, c.get(1));
                        c2.set(1, c.get(2));
                        c2.set(2, c.get(3));
                        c2.set(3, c.get(0));
                    }
//                    std::cout<< "after: "
//                             << +c2.get(0) << " " << +c2.get(1)
//                              << " " << +c2.get(2) << " " << +c2.get(3)
//                              << std::endl << std::endl;
                }


//                auto& left = g2.getNode(x - 1, y);
//                auto& right = g2.getNode(x + 1, y);
//                auto& top = g2.getNode(x, y+1);
//                auto& bottom = g2.getNode(x, y-1);

//                bottom.set(0, c.get(0)); c.set(0, 0);
//                left.set  (1, c.get(1)); c.set(1, 0);
//                top.set   (2, c.get(2)); c.set(2, 0);
//                right.set (3, c.get(3)); c.set(3, 0);
            }
        }

        std::swap(grid, g2);

    }

    void updateImage()
    {
        int width = grid.getXsize();
        int height = grid.getYsize();

        //std::vector<GLubyte> image(4*width*height);
        //std::fill(image.begin(), image.end(), 0);
        #pragma omp parallel for
        for(int j = 0; j < height; ++j)
        {
            for(int i = 0;i<width;++i)
            {
                size_t index = j*width + i;

                auto d = grid.getNode(i, j).get_density();
                auto v = 63 * d;
                image[4*index + 0] = v; // R
                image[4*index + 1] = v; // G
                image[4*index + 2] = v;// B
                image[4*index + 3] = 255; // A
            }
        }
        glBindTexture(GL_TEXTURE_2D, t);    //A texture you have already created storage for
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_BGRA, GL_UNSIGNED_BYTE, image.data());

    }

    GLuint createTexture()
    {
        GLuint tex;

        int width = grid.getXsize();
        int height = grid.getYsize();

        for(int j = 0; j < height; ++j)
        {
            for(int i = 0;i<width;++i)
            {
                size_t index = j*width + i;

                auto d = grid.getNode(i, j).get_density();
                auto v = 63 * d;
                image[4*index + 0] = v; // R
                image[4*index + 1] = v; // G
                image[4*index + 2] = v;// B
                image[4*index + 3] = 0; // A
            }
        }


        // allocate a texture name
        glGenTextures( 1, &tex );

        // select our current texture
        glBindTexture( GL_TEXTURE_2D, tex );

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);


        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
                     GL_RGBA, GL_UNSIGNED_BYTE, image.data());

        return tex;
    }
};
#endif
