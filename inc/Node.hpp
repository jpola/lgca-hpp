#ifndef __NODE_HPP_
#define __NODE_HPP_

#include<vector>
#include<cassert>

class Node
{
public:
    Node() : _particles(N)
    {
        for(auto& p : _particles)
            p = 0;
    }

    Node& operator=(const Node& n)
    {
        _particles = n._particles;
    }

    void set(const int i, unsigned char value)
    {
        assert(i < N);
        assert(value < 2);
        _particles[i] = value;

    }

    unsigned char get(const int i)
    {
        assert(i < N);
        return _particles[i];
    }

    unsigned char get_density() const
    {
        unsigned char sum = 0;
        for(auto& p : _particles)
            sum += p;

        return sum;
    }

private:
    const int N = 4;
    std::vector<unsigned char> _particles;
};

#endif //__NODE_HPP_
